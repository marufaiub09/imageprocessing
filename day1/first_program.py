# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# Understanding digital Image processing

# %%
from skimage import io
import numpy as np
from matplotlib import pyplot as plt
from skimage import img_as_float

my_image = io.imread("images/Landscape.jpg")
print(my_image.min(), my_image.max())
#plt.imshow(my_image)
my_image[10:400, 10:400, :] = [255, 255, 0]
plt.imshow(my_image)


