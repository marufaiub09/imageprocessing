# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# Reading Images in Python

# %%
from PIL import Image
import numpy as np

img = Image.open("images/Landscape.jpg")
print(type(img))

img.format

#convert pillow to numpy array
img1 = np.asarray(img)
print(type(img1))

# %% [markdown]
# #matplotlib
# #Pyplot[creates and generates 2D plot]

# %%
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

mat_img = mpimg.imread("images/Landscape.jpg")
print(type(mat_img))
print(mat_img.shape)
plt.imshow(mat_img)
plt.colorbar()

# %% [markdown]
# #Scikit Image - a image pr lib use for image segementation, geometric transformation, color space manipulation, analysis, filtering, feature detection

# %%
from skimage import io, img_as_float, img_as_uint, img_as_ubyte
import matplotlib.pyplot as plt

image = io.imread("images/Landscape.jpg")
#print(image)
#plt.imshow(image)
print(image.min(), image.max())
image_float = img_as_float(image)
#print(image_float.min(), image_float.max())
#print(image_float)

# %% [markdown]
# #openCV - programming function mainly aimed at computer vision.Applications - facial detection, OCR, motion detection, live video, artificial neural network.

# %%
import cv2
import matplotlib.pyplot as plt

grey_img = cv2.imread("images/Landscape.jpg", 0)
color_img = cv2.imread("images/Landscape.jpg", 1)
#plt.imshow(grey_img)
#image convert form BGR to RGB
plt.imshow(cv2.cvtColor(color_img, cv2.COLOR_BGR2RGB))



cv2.imshow("Grey Image", grey_img)
cv2.imshow("Color Image", color_img)
cv2.waitKey(1000)
cv2.destroyAllWindows()

# %% [markdown]
# #CZI file

# %%
import czifile
img = czifile.imread("images/test_image.czi")
print(img.shape)

# %% [markdown]
# #OME-TIFF

# %%
from apeer_ometiff_library import io

(pic2, omexml) = io.read_ometiff("images/test_image.ome.tif")
print(pic2.shape)
print(pic2)
print(omexml)

# %% [markdown]
# # read many file in a folder using glob

# %%
import cv2
import glob2

path = "images/*"

for file in glob2.glob(path):
    print(file)
    a = cv2.imread(file)
    print (a)
    # cv2.imshow("Original Image", a)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    
    c = cv2.cvtColor(a, cv2.COLOR_BGR2RGB)
    cv2.imshow("Color Image", c)
    cv2.waitKey(100000)
    cv2.destroyAllWindows()


